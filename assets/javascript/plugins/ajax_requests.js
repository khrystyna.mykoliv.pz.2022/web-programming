// ================================ AJAX GET STUDENTS FROM DB ================================

function getStudents() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "GET",
            url: "/students",
            cache: false,
            success: function(data) {
                data.forEach(element => {
                    students.push({
                        id: element._id,
                        checked: false,
                        group: element.group,
                        name: element.name,
                        gender: element.gender,
                        birthday: element.birthday.replace(/^(\d{4})-(\d{2})-(\d{2})$/, "$3.$2.$1"),
                        status: element.status
                    })
                });

                resolve();
            },
            error: function(xhr, status, error) {
                console.error("Error:", error);
                reject();
            }
        })
    })
}

// ================================ AJAX POST STUDENT ================================

function postStudentToServer(student) {
    $.ajax({
        type: "POST",
        url: '/students',
        cache: false,
        data: JSON.stringify(student),
        contentType: 'application/json',
        success: function(data) {
            const newData = { id: data._id, ...data };
            delete newData._id;
            let studentNode = document.createElement('tr');
            studentNode.id = String(newData.id);
            studentNode.innerHTML = toHtml(newData);
            parentStudents.append(studentNode);
            students.push(newData);
            let remove_button = document.querySelector(`button[data-id="${newData.id}"][data-btn="remove"]`);
            let edit_button = document.querySelector(`button[data-id="${newData.id}"][data-btn="edit"]`);

            remove_button.addEventListener('click', handleRemoveButtonClick);
            edit_button.addEventListener('click', handleEditButtonClick);

            lib.flash({
                type: 'success',
                message: 'Student was successfully created'
            });
        },
        error: function(xhr, status, error) {
            let response = JSON.parse(xhr.responseText);
            let errors = response.errors;

            if (errors) {
                for (let field in errors) {
                    if (errors.hasOwnProperty(field) && errors[field] !== null) {
                        lib.flash({
                            type: 'fail',
                            message: errors[field]
                        });
                    }
                }
            }
        }
    });
}

// ================================ AJAX PUT STUDENT ================================

function editStudentToServer(student) {
    $.ajax({
        type: "PUT",
        url: `/students/${student.id}`,
        data: JSON.stringify(student),
        contentType: 'application/json',
        success: function(data) {
            let studentNode = $(`#${data._id}`);
            studentNode.children('.student-group').text(data.group);
            studentNode.children('.student-name').text(data.name);
            studentNode.children('.student-gender').text(data.gender);
            studentNode.children('.student-birthday').text(moment.utc(data.birthday).format('DD.MM.YYYY'));

            lib.flash({
                type: 'success',
                message: 'Student was successfully updated'
            });
        },
        error: function(xhr, status, error) {
            let response = JSON.parse(xhr.responseText);
            let errors = response.errors;

            if (errors) {
                for (let field in errors) {
                    if (errors.hasOwnProperty(field) && errors[field] !== null) {
                        lib.flash({
                            type: 'fail',
                            message: errors[field]
                        });
                    }
                }
            }
        }
    });
}

// ================================ AJAX DELETE STUDENT ================================

function deleteStudentToServer(id, indexToRemove) {
    $.ajax({
        type: "DELETE",
        url: `/students/${id}`,
        contentType: 'application/json',
        success: function(data) {
            let nodeToRemove = $(`#${String(id)}`);
            parentStudents.find(nodeToRemove).remove();
            $('.parent .child').remove();
            students.splice(indexToRemove, 1);

            lib.flash({
                type: 'success',
                message: 'Student was successfully removed'
            });
        },
        error: function(xhr, status, error) {
            lib.flash({
                type: 'fail',
                message: 'Student was failed removed!'
            });
        }
    });
}
