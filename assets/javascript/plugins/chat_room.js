let currentChat = {
    getStatus: () => {
        this.status = document.getElementById('chat-room') !== null;
    }
}

function _createMembers(members = []) {
    let wrap;

    if (document.getElementById('members-chat') === null) {
        wrap = document.createElement('div');
        wrap.classList.add('members');
        wrap.id = 'members-chat';
    } else {
        wrap = document.getElementById('members-chat');
        wrap.innerHTML = "";
    }

    const filteredMembers = [];
    for (const item of members) {
        if (item !== null) {
            filteredMembers.push(item);
        }
    }
    if (filteredMembers.length !== 0) {
        filteredMembers.forEach(member => {
            const $member = document.createElement('div');
            $member.classList.add('member');
            $member.innerHTML = `<img src="${member.avatar}" alt="chat-user">`;
            wrap.appendChild($member);
        });
    }

    const $btnAddMember = document.createElement('button');
    $btnAddMember.classList.add('add__member');
    $btnAddMember.id = 'add__member';
    wrap.appendChild($btnAddMember);

    $btnAddMember.addEventListener('click', newMembers);

    return wrap;
}

function newMembers() {
    socket.emit('availableMembersGroup', { group: currentChat.backName });
    socket.once('available-users', (availableUsers) => {
        lib.members_modal({
            title: 'Add member',
            closable: true,
            availableUsers: availableUsers,
            width: window.innerWidth < 600 ? '90%' : '550px'
        }).then((data) => {
            socket.emit('addMembersToGroup', { users: data, group: currentChat.backName });

            socket.on('groupMembersList', ({ chatMembers }) => {
                const members = _createMembers(chatMembers);
                members.appendAfter(currentChat.chat.querySelector('[data-members]'));
            });
        }).catch((err) => {
            console.log('Error in form members');
        });
    })
}

function _createFooter() {
    const wrap = document.createElement('div');
    wrap.classList.add('chat__room-footer');

    const $inputMessage = document.createElement('input');
    $inputMessage.type = "text";
    $inputMessage.id = `chat__message${currentUser.name}`;
    $inputMessage.classList.add('standard__input');
    $inputMessage.placeholder = 'Type message...';
    wrap.appendChild($inputMessage);

    const $btnSendMessage = document.createElement('button');
    $btnSendMessage.id = `send__message-button${currentUser.name}`;
    $btnSendMessage.classList.add('send__message');
    $btnSendMessage.innerHTML = `<img src="../../assets/images/send-message-icon.svg" alt="Send Message">`;
    wrap.appendChild($btnSendMessage);

    return wrap;
}

function _createChat(options) {
    const chatNode = document.createElement('div');
    chatNode.classList.add('main-chat');
    chatNode.id = `chat-room${currentUser.name}`;
    chatNode.innerHTML = `
        ${options.type !== 'private' ?
        `<h4 class="main-chat__title" id="chat-title">Chat room ${options.name}</h4>` :
        `<h4 class="main-chat__title" id="chat-title">
            Private chat with ${options.name.replace(currentUser.name, '').replace('private', '').split('_').join('')}
        </h4>`
    }
        ${options.type !== 'private' ? 
        `<div class="main-chat__members">
            <h4 class="main-chat__title" data-members>Members</h4>
        </div>` : ''
        }
        <div class="chat__room">
            <h4 class="main-chat__title">Messages</h4>
            <div class="chat__messages" id="chat__messages" data-messages></div>
        </div>
    `;

    const footer = _createFooter();
    footer.appendAfter(chatNode.querySelector('[data-messages]'));

    const menuChat = document.getElementById('menu-chat');
    menuChat.appendChild(chatNode);

    return chatNode;
}

document.addEventListener("DOMContentLoaded", function() {
    lib.chat = function(options) {
        let chat;
        if (document.getElementById(`chat-room${currentUser.name}`)) {
            clearChat();
        }

        chat = _createChat(options);

        currentChat.chat = chat;
        currentChat.type = options.type;
        currentChat.name = options.name;
        currentChat.backName = options.backName;

        if (currentChat.type !== 'private') {
            socket.on('groupMembersList', ({ chatMembers }) => {
                const members = _createMembers(chatMembers);
                members.appendAfter(chat.querySelector('[data-members]'));
            });
        }

        let messageInput = document.getElementById(`chat__message${currentUser.name}`);
        const chatMessages = document.getElementById('chat__messages');
        let sendMessageButton = document.getElementById(`send__message-button${currentUser.name}`);

        socket.on('chat-message', (data) => {
            if (currentUser.name === data.sender.name || data.room !== currentChat.backName) {
                return;
            }

            addMessageToUi(false, data);
        });

        socket.once('archivedMessages', ({ messages }) => {
            console.log('archivedMessages', messages);
            if (Object.keys(messages).length === 0) { return; }
            messages.forEach(message => {
                let isOwnMessage = false;

                if (message.sender.name === currentUser.name) {
                    isOwnMessage = true;
                }

                addMessageToUi(isOwnMessage, message);
            })
        });

        function sendMessage() {
            const data = {
                sender: { name: currentUser.name, avatar: currentUser.avatar },
                content: messageInput.value.trim(),
                room: currentChat.backName,
                date: new Date()
            }

            if (data.message === "") return

            socket.emit('message', data);
            addMessageToUi(true, data);
        }

        sendMessageButton.addEventListener('click', sendMessage);

        messageInput.addEventListener('keypress', (e) => {
            socket.emit('feedback', {
                feedback: `✍️ ${currentUser.name} is typing a message...`,
                room: currentChat.backName,
                name: currentUser.name
            })

            if (e.keyCode === 13) {
                sendMessage()
            }
        })

        messageInput.addEventListener('blur', (e) => {
            socket.emit('feedback', {
                feedback: ``,
                room: currentChat.backName,
                name: currentUser.name
            })
        })

        messageInput.addEventListener('input', (e) => {
            if (messageInput.value.trim() === '') {
                socket.emit('feedback', {
                    feedback: ``,
                    room: currentChat.backName,
                    name: currentUser.name
                });
            }
        });

        messageInput.addEventListener('focus', (e) => {
            if (messageInput.value.trim() === '') {
                socket.emit('feedback', {
                    feedback: ``,
                    room: currentChat.backName,
                    name: currentUser.name
                });
            }
        });

        socket.on('feedback', (data) => {
            if (currentUser.name === data.name || data.room !== currentChat.backName) {
                return;
            }

            clearFeedback();
            const element = `
            <div class="message__feedback">
                <div class="feedback" id="feedback">${data.feedback}</div>
            </div>
        `

            chatMessages.innerHTML += element;
        })

        function addMessageToUi(isOwnMessage, data) {
            clearFeedback();
            const avatarElement = isOwnMessage ? '' :
                `<div class="message__avatar">
                    <img src="${data.sender.avatar}" alt="notification-avatar"/>
                </div>`;

            const element = `
            <div class="message ${isOwnMessage ? 'my-message' : ''}">
                ${avatarElement}
                <div class="message__info">
                    ${isOwnMessage ? '' : `<h6 class="message__author">${data.sender.name}</h6>`}
                    <div class="message__text">${data.content}</div>
                    <span class="message__date">${moment(data.timestamps).fromNow()}</span>
                </div>
            </div>
        `

            chatMessages.innerHTML += element;
            messageInput.value = "";
            chatMessages.scrollTop = chatMessages.scrollHeight;
        }

        function clearFeedback() {
            document.querySelectorAll('div.message__feedback').forEach(element => {
                element.parentElement.removeChild(element);
            })
        }

        function clearChat() {
            const chatRoomElement = document.getElementById(`chat-room${currentUser.name}`);
            let messageInput = document.getElementById(`chat__message${currentUser.name}`);
            let sendMessageButton = document.getElementById(`send__message-button${currentUser.name}`);
            let addMember = document.getElementById('add__member');
            sendMessageButton.removeEventListener('click', sendMessage);
            if (addMember) {
                addMember.removeEventListener('click', newMembers);
            }
            messageInput.removeEventListener('keypress', null);
            messageInput.removeEventListener('blur', null);
            messageInput.removeEventListener('input', null);
            messageInput.removeEventListener('focus', null);
            chatRoomElement.remove();

            const events = ['chat-message', 'feedback', 'archivedMessages'];

            events.forEach(event => {
                socket.off(event);
            });
        }
    }
});
