import mongoose from 'mongoose';

const studentSchema = new mongoose.Schema({
    group: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    gender: {
        type: String,
        enum: ['M', 'F'],
        required: true
    },
    birthday: {
        type: Date,
        required: true
    },
    status: {
        type: Boolean,
        default: false
    }
});


const Student = mongoose.model('Student', studentSchema);

export default Student;