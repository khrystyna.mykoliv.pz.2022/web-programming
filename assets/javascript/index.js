let students = []
const parentStudents = document.getElementById('students-items');

const toHtml = student => `
    <td><input data-id='${student.id}' class="check-input" type="checkbox" ${student.checked ? 'checked' : ''}></td>
    <td class="student-group">${student.group}</td>
    <td class="student-name">${student.name}</td>
    <td class="student-gender">${student.gender}</td>
    <td class="student-birthday">${student.birthday}</td>
    <td class="student-status"><div class="circle-status${student.status ? ' active' : ''}"></div></td>
    <td class="student-options">
        <div class="student-actions">
            <button class="btn img btn-outline-primary btn__edit" data-btn="edit" data-id='${student.id}'>
                <img src="../images/edit-icon.svg" alt="edit-icon">
            </button>
            <button class="btn img btn-outline-danger btn__remove" data-btn="remove" data-id='${student.id}'>
                <img src="../images/delete-icon.svg" alt="delete-icon">
            </button>
        </div>
    </td>
`

// ================================ RENDER STUDENTS ================================

getStudents().then(() => {
    render();

    removeStudent();
    editStudent();
})

function render() {
    students.forEach((student) => {
        let studentNode = document.createElement('tr');
        studentNode.id = String(student.id);
        studentNode.innerHTML = toHtml(student);
        parentStudents.append(studentNode);
    });
}

// ================================ DELETE STUDENT ================================

function handleRemoveButtonClick(event) {
    event.preventDefault();

    let id = Number(event.currentTarget.dataset.id);
    let indexToRemove = students.findIndex(student => student.id === id);
    const modalWindow = lib.confirm({
        title: 'Warning',
        closable: true,
        width: window.innerWidth < 600 ? '90%' : '550px',
        content: `<p>Are you sure you want to delete user <strong>${students[indexToRemove].name}</strong>?</p>`
    }).then(() => {
        deleteStudentToServer(id, indexToRemove);
    }).catch(() => {
        console.log('Decline');
    });
}

function removeStudent() {
    document.querySelectorAll(".btn__remove").forEach(button => {
        button.addEventListener('click', handleRemoveButtonClick);
    });
}

removeStudent();

// ================================ NEW STUDENT ================================

$('#add__student').on('click', () => {
    lib.student_form({
        title: 'New student',
        closable: true,
        button_action: 'Create',
        width: window.innerWidth < 600 ? '90%' : '550px'
    }).then((data) => {
        let student =  {
            id: students.length === 0 ? 1 : Math.max(...students.map(student => student.id)) + 1,
            checked: false,
            status: false,
            ...data
        };

        postStudentToServer(student);
    }).catch((err) => {
        console.log(err);
    });
})

// ================================ EDIT STUDENT ================================
function handleEditButtonClick(event) {
    event.preventDefault();

    let id = Number(event.currentTarget.dataset.id);
    let indexToEdit = students.findIndex(student => student.id === id);
    lib.student_form({
        title: 'Edit Student',
        closable: true,
        student: students[indexToEdit],
        button_action: 'Save',
        width: window.innerWidth < 600 ? '90%' : '550px'
    }).then((data) => {
        students[indexToEdit] = { ...students[indexToEdit], ...data };
        editStudentToServer(students[indexToEdit]);
    }).catch((err) => {
        console.log(err);
    });
}

function editStudent() {
    document.querySelectorAll(".btn__edit").forEach(button => {
        button.addEventListener('click', handleEditButtonClick);
    });
}

editStudent();

// ================================ NOTIFICATION ================================

$('#notification-icon').hover(() => {
    $('#notification-icon').addClass('animation');

    setTimeout(() => {
        $('#notification-icon').removeClass('animation');
    }, 500);
})
