import crypto from 'crypto';

const generateSecretToken = () => {
    return crypto.randomBytes(64).toString('hex');
};

const newSecretToken = generateSecretToken();
console.log(`New SECRET_ACCESS_TOKEN: ${newSecretToken}`);
