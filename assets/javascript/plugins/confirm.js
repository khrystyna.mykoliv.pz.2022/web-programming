lib.confirm = function(options) {
    return new Promise((resolve, reject) => {
        const modal = lib.modal({
            title: options.title,
            width: options.width,
            closable: options.closable,
            content: options.content,
            onClose() {
                modal.destroy();
            },
            footerButtons: [
                { text: 'Ok', type: 'success', handler() {
                    modal.close();
                    resolve();
                }},
                { text: 'Cancel', type: 'danger', handler() {
                    modal.close();
                    reject();
                }}
            ]
        });

        setTimeout(() => {modal.open()}, 300);
    });
}
