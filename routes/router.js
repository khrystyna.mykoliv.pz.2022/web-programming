import {Router} from "express";
const router = Router();
import mongoose from 'mongoose';
import { SECRET_ACCESS_TOKEN } from "../index.js";
import jwt from 'jsonwebtoken';
import User from "../models/User.js";
import Task from "../models/Task.js";
import Student from "../models/Student.js";
import path from "path";
import multer from "multer";

const __dirname = path.resolve();
const uploadDir = path.join(__dirname, 'uploads');

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, uploadDir);
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname);
    }
});

const upload = multer({ storage: storage });

const CheckToken = (req, res, next) => {
    if (!req.cookies) {
        res.redirect("/auth/log_in");
    }
    const token = req.cookies.token;
    if(!token) {
        res.redirect("/auth/log_in");
    }
    try {
        const decoded = jwt.verify(token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' });
        console.log(decoded)
        req.userId = decoded.userId;
        next();
    } catch(error) {
        res.status(401).json({message: 'Unauthorized'});
    }
}

router.get('/logout',(req,res)=>{
    res.clearCookie('token');
    res.redirect("/");
});

router.get('/auth/sign_up', (req, res) => {
    res.render('register', {title: 'Register', active: 'register'})
});

router.get('/auth/log_in', (req, res) => {
    res.render('login', {title: 'Login', active: 'login'})
});

router.get('/', CheckToken, (req, res) => {
    const userId = jwt.verify(req.cookies.token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' }).userId;
    console.log(userId);
    User.findOne({ _id: userId }).then(userDB => {
        res.render('index', {title: 'Main Page', active: 'main', user: userDB });
    }).catch(error => {
        console.log("Error", error);
    });
});

router.get('/tasks', CheckToken, async (req, res) => {
    const userId = jwt.verify(req.cookies.token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' }).userId;
    try {
        await Task.updateMany(
            { viewed: { $ne: userId } },
            { $addToSet: { viewed: userId } }
        );

        console.log('Tasks updated successfully.');
    } catch (error) {
        console.error('Error updating tasks:', error);
        throw error;
    }

    User.findOne({ _id: userId }).then(userDB => {
        Task.find({}).then(tasks => {
            const todo_tasks = tasks.filter(item => String(item.board) === 'todo');
            const in_process_tasks = tasks.filter(item => String(item.board) === 'in_process');
            const done_tasks = tasks.filter(item => String(item.board) === 'done');
            res.render('tasks/tasks', {
                title: 'Tasks',
                active: 'tasks',
                user: userDB,
                todo_tasks: todo_tasks,
                in_process_tasks: in_process_tasks,
                done_tasks: done_tasks
            });
        })
    }).catch(error => {
        console.log("Error", error);
    });
});

router.get('/tasks/new', CheckToken, (req, res) => {
    const userId = jwt.verify(req.cookies.token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' }).userId;
    User.findOne({ _id: userId }).then(userDB => {
        res.render('tasks/new_task', {title: 'Tasks', active: 'tasks', user: userDB });
    }).catch(error => {
        console.log("Error", error);
    });
});

router.post('/tasks',  async (req, res) => {
    const { board, name, date, description } = req.body;

    try {
        const userId = jwt.verify(req.cookies.token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' }).userId;
        const task = new Task({ board, name, description, date, viewed: [userId] });
        await task.save();
        res.redirect('/tasks');
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.get('/tasks/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const userId = jwt.verify(req.cookies.token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' }).userId;
        const task = await Task.findById(id);
        User.findOne({ _id: userId }).then(userDB => {
            res.render('tasks/update_task', {title: 'Tasks', active: 'tasks', user: userDB, task: task });
        }).catch(error => {
            console.log("Error", error);
        });
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.post('/tasks/:id', async (req, res) => {
    const { id } = req.params;
    const { board, name, date, description } = req.body;

    try {
        const task = await Task.findByIdAndUpdate(
            id,
            { board, name, date, description },
            { new: true });
        res.redirect('/tasks');
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.get('/chat', CheckToken, (req, res) => {
    const userId = jwt.verify(req.cookies.token, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' }).userId;
    User.findOne({ _id: userId }).then(userDB => {
        res.render('chat', {title: 'Chat', active: 'chat', user: userDB})
    }).catch(error => {
        console.log("Error", error);
    });
});

router.post('/auth/login', async (req, res) => {
    const { email, password } = req.body;
    console.log(req.body);
    try {
        const user = await User.findOne({ email: email }).exec();
        if (user) {
            const id = new mongoose.Types.ObjectId(user._id);
            console.log(id);
            const token = jwt.sign({ userId: id }, SECRET_ACCESS_TOKEN, { algorithm: 'HS256' });
            res.cookie('token', token, { httpOnly: true });
            res.redirect('/');
        } else {
            res.redirect('/auth/log_in');
        }
    } catch (error) {
        console.error('Error finding user:', error);
        res.redirect('/auth/log_in');
    }
});

router.get('/students', async (req, res) => {
    try {
        const students = await Student.find({});
        res.send(students);
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.post('/students',  async (req, res) => {
    const { status, group, name, gender, birthday } = req.body;

    try {
        const student = new Student({ status, group, name, gender, birthday });
        await student.save();
        res.send(student);
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.put('/students/:id', async (req, res) => {
    const { id } = req.params;
    const { status, group, name, gender, birthday } = req.body;

    console.log(parseDateString(birthday));
    try {
        const student = await Student.findByIdAndUpdate(
            id,
            { status, group, name, gender, birthday: parseDateString(birthday) },
            { new: true });
        res.send(student);
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.delete('/students/:id', async (req, res) => {
    const { id } = req.params;

    try {
        const user = await Student.findByIdAndDelete(id);
        res.send('success');
    } catch (error) {
        console.error(error);
        res.status(500).send(error);
    }
});

router.post('/auth/register', upload.single('avatar'), (req, res)=> {
    const { email, name, password } = req.body;
    const avatarPath = req.file;

    if (!avatarPath) return res.sendStatus(400);

    const basePath = '/Users/khrystynamykoliv/website/';
    const avatar = '../' + avatarPath.path.replace(basePath, '');
    const new_user = add_user({email, name, password, avatar});
    console.log(new_user);
    const token = jwt.sign({userId: new_user._id}, SECRET_ACCESS_TOKEN);
    res.cookie('token', token, {httpOnly: true});
    res.redirect("/chat", {user: new_user});
})

function add_user(user) {
    try {
        const newUser =  User.create({
            email: user.email,
            name: user.name,
            password: user.password,
            avatar: user.avatar
        });

        console.log('Користувач доданий успішно:', newUser);
        return newUser;
    } catch (error) {
        console.error('Помилка при додаванні користувача:', error);
    }
}

function parseDateString(dateString) {
    const parts = dateString.split('.');
    const day = parseInt(parts[0], 10);
    const month = parseInt(parts[1], 10) - 1;
    const year = parseInt(parts[2], 10);
    return new Date(Date.UTC(year, month, day));
}

export default router