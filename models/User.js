import mongoose from 'mongoose';

const userSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            unique: true,
            required: "Your full name is required"
        },
        email: {
            type: String,
            required: "Your email is required",
            unique: true,
            lowercase: true,
            trim: true,
        },
        password: {
            type: String,
            required: "Your password is required",
            select: false,
            max: 25,
        },
        avatar: {
            type: String,
            default: ''
        }
    }, { timestamps: true }
);

const User = mongoose.model('User', userSchema);

export default User;