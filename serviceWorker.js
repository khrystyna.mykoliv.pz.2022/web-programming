// const staticCacheName = 's-app-v1'
//
// const assetUrls = [
//     'index.php',
//     'app.js',
//     'style.css',
//     'javascript/plugins/ajax_requests.js',
//     'javascript/base.js',
//     'javascript/index.js',
//     'javascript/plugins/confirm.js',
//     'javascript/plugins/flash.js',
//     'javascript/plugins/form_student.js',
//     'javascript/plugins/modal.js',
//     'images/youravatar.svg',
//     'images/delete-icon.svg',
//     'images/edit-icon.svg',
//     'images/notification_logo.svg',
//     'images/students.png',
//     'images/Skull.svg'
// ]
//
// self.addEventListener('install', async event => {
//     const cache = await caches.open(staticCacheName);
//     await cache.addAll(assetUrls);
// })
//
// self.addEventListener('activate', async vent => {
//     const cacheNames = await caches.keys()
//     await Promise.all(
//         cacheNames
//             .filter(name => name !== staticCacheName)
//             .map(name => caches.delete((name)))
//     )
// })
//
// self.addEventListener('fetch', event => {
//     event.respondWith(cacheFirst(event.request))
// })
//
// async function cacheFirst(request) {
//     const cached = await caches.match(request)
//     return cached ?? await fetch(request)
// }