import cookieParser from "cookie-parser";
import bodyParser from "body-parser";

import dotenv from 'dotenv';
import express from "express";
import path from 'path';
import {Server} from 'socket.io';
import mongoose from 'mongoose';
import User from './models/User.js';
import Chat from './models/Chat.js';
import Message from './models/Message.js';
import Router from "./routes/router.js";
import {PORT, URI} from "./index.js";
import jwt from "jsonwebtoken";
import moment from "moment";
import Task from "./models/Task.js";

dotenv.config();

mongoose.connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}).then(() => {
    console.log("Database connected successfully")
}).catch(error => {
    console.log("Mongo connection failed", error)
});

const __dirname = path.resolve();
const app = express();
const port = PORT;

app.use((req, res, next)=>{
    res.locals.moment = moment;
    next();
})

app.set('view engine', 'ejs');
app.set('views', path.resolve(__dirname, 'ejs'))
app.use('/assets', express.static(path.join(__dirname, 'assets')))
app.use('/uploads', express.static(path.join(__dirname, 'uploads')))
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(Router)

// ================================ CODE ================================

const server = app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});

const io = new Server(server);
const ActiveUsers = {
    users: [],
    setUsers: function(newArrayUsers) {
        this.users = newArrayUsers;
    }
}

const ActiveRooms = {
    rooms: [],
    setRooms: function(newArrayRooms) {
        this.rooms = newArrayRooms;
    }
}

io.on('connection', async (socket) => {
    try {
        const cookieHeader = socket.handshake.headers.cookie;

        if (!cookieHeader) {
            console.error("Cookie header is missing in socket handshake.");
            // Optionally, you can terminate the connection or send an error message
            return;
        }

        const tokenCookie = cookieHeader.split('; ').find(cookie => cookie.trim().startsWith('token='));

        if (!tokenCookie) {
            console.error("Token cookie is missing.");
            // Optionally, you can terminate the connection or send an error message
            return;
        }

        const token = tokenCookie.split('=')[1];
        const decoded = jwt.verify(token, process.env.SECRET_ACCESS_TOKEN, { algorithm: 'HS256' });
        const userId = new mongoose.Types.ObjectId(decoded.userId);

        if (!token) throw new Error('Token not found');

        const userDB = await User.findOne({ _id: userId });
        if (!userDB) throw new Error('User not found');

        activateUser(socket.id, userDB.id, userDB.name, userDB.avatar);
        io.to(socket.id).emit('currentUser', { user: userDB });

        const rooms = await getAllChatsForUser(userId);
        const unViewedMessages = await getAllUnViewedMessagesForUser(userId);
        const unViewedTasks = await getAllUnViewedTasksForUser(userId);
        io.to(socket.id).emit('roomList', { rooms });
        io.to(socket.id).emit('unReadMessages', { unViewedMessages });
        io.to(socket.id).emit('unViewedTasks', { unViewedTasks });

        console.log(rooms);
        const users = await User.find({});
        for (const user of users) {
            if (user._id !== userId.toString()) {
                const existingChat = await Chat.findOne({
                    members: { $all: [userId, user._id] }, type: 'private'
                });

                if (!existingChat) {
                    const newChat = new Chat({
                        members: [userId, user._id],
                        type: 'private',
                        name: `private_${userDB.name}_${user.name}`
                    });
                    await newChat.save();
                }
            }
        }

        socket.on('EnterPrivateChat', async ({ sender, receiver }) => {
            try {
                const userSender = await User.findOne({ name: sender });
                const userReceiver = await User.findOne({ name: receiver });
                const existingChat = await Chat.findOne({
                    members: { $all: [userSender.id, userReceiver.id] }
                }).populate('messages');

                socket.join(existingChat.name);

                // Оновлення переглянутих повідомлень для поточного користувача
                const messages = await getAllMessages(existingChat);

                await updateViewedMessagesForUser(userId, existingChat.messages);

                io.to(socket.id).emit('privateChatCreated', { sender, roomName: existingChat.name });
                io.to(socket.id).emit('archivedMessages', { messages: messages });
            } catch (error) {
                console.error('Error entering private chat:', error);
            }
        });

        socket.on('createGroup', async ({ name, room }) => {
            const owner = await User.findOne({name: name});

            const newChat = new Chat({
                members: [owner.id],
                type: 'group',
                name: room
            });
            const createdChat = await newChat.save();

            const memberPromises = createdChat.members.map(async id => {
                return User.findOne({_id: id});
            });

            const members = await Promise.all(memberPromises);

            socket.join(createdChat.name);
            io.to(createdChat.name).emit('GroupCreated', { name, room });
            io.to(createdChat.name).emit('addGroupInList', { group: createdChat.name, user: owner });
            io.to(socket.id).emit('groupMembersList', { chatMembers: members });
        });

        socket.on('EnterGroup', async ({ name, room }) => {
            const existingChat = await Chat.findOne({ name: room }).populate('messages');

            socket.join(existingChat.name);

            const memberPromises = existingChat.members.map(async id => {
                return User.findOne({_id: id});
            });

            const members = await Promise.all(memberPromises);
            const messages = await getAllMessages(existingChat);

            await updateViewedMessagesForUser(userId, existingChat.messages);

            io.to(socket.id).emit('GroupCreated', { name, room });
            io.to(socket.id).emit('archivedMessages', { messages: messages });
            io.to(socket.id).emit('groupMembersList', { chatMembers: members });
        });

        socket.on('message', async (data) => {
            try {
                const existingChat = await Chat.findOne({
                    name: data.room
                });

                const newMessage = new Message({
                    chat: existingChat.id,
                    sender: userId,
                    viewed: [userId],
                    content: data.content
                });
                const savedMessage = await newMessage.save();

                await existingChat.updateOne( { $push: { messages: savedMessage.id }});

                io.to(data.room).emit('chat-message', data);
            } catch (error) {
                console.error('Error sending message:', error);
            }
        });

        socket.on('availableMembersGroup', async (group) => {
            const existingChat = await Chat.findOne({ name: group.group });
            const availableUsers = await User.find({ _id: { $nin: existingChat.members } });
            console.log(availableUsers);

            io.to(socket.id).emit('available-users', availableUsers);
        })

        socket.on('addMembersToGroup', async ({ users, group }) => {
            const existingChat = await Chat.findOne({ name: group });
            const usersData = await User.find({ name: { $in: users } });
            const usersDB = usersData.map(user => ({ _id: user._id, name: user.name, avatar: user.avatar }));

            existingChat.members.push(...usersDB.map(user => user._id));
            await existingChat.save();

            const memberPromises = existingChat.members.map(async id => {
                return User.findOne({_id: id});
            });

            const members = await Promise.all(memberPromises);

            io.to(socket.id).emit('groupMembersList', { chatMembers: members });
        });

        socket.on('feedback', (data) => {
            io.to(data.room).emit('feedback', data);
        })

        socket.on('disconnect', () => {
            roomLeavesApp(socket.id);
            userLeavesApp(socket.id);
        });

    } catch (error) {
        console.error('Socket connection error:', error);
    }
});

async function getAllChatsForUser(userId) {
    try {
        return await Chat.find({members: userId}).populate('members').populate('messages');
    } catch (error) {
        console.error('Error fetching chats:', error);
        throw error;
    }
}

// ================================ FUNCTIONS ================================

function activateUser(id, db_id, name, avatar) {
    const user = { id, db_id, name, avatar }
    ActiveUsers.setUsers([
        ...ActiveUsers.users.filter(user => user.id !== id),
        user
    ])

    return user;
}

function userLeavesApp(id) {
    ActiveUsers.setUsers(
        ActiveUsers.users.filter(user => user.id !== id)
    )
}

function roomLeavesApp(id) {
    let user = getUser(id);

    ActiveRooms.setRooms(
    ActiveRooms.rooms.filter(room => !room.users.includes(user.name))
    )
}

async function updateViewedMessagesForUser(currentUserid, messages) {
    try {
        for (const message of messages) {
            if (!message.viewed.includes(currentUserid)) {
                message.viewed.push(currentUserid);
                await message.save();
            }
        }
    } catch (error) {
        console.error('Error updating viewed messages:', error);
    }
}

async function getAllMessages(chat) {
    const messages = await Message.find({ chat: chat._id }).populate('sender');

    return messages.map(message => ({
        _id: message._id,
        chat: message.chat,
        sender: {
            _id: message.sender._id,
            name: message.sender.name,
            avatar: message.sender.avatar
        },
        content: message.content,
        timestamp: message.timestamp,
        viewed: message.viewed
    }));
}

async function getAllUnViewedMessagesForUser(user_id) {
    try {
        const userChats = await Chat.find({ members: user_id }).populate({
            path: 'messages',
            populate: {
                path: 'sender',
                model: 'User'
            }
        });

        const unViewedMessages = [];

        for (const chat of userChats) {
            for (const message of chat.messages) {
                if (!message.viewed.includes(user_id)) {
                    unViewedMessages.push(message);
                }
            }
        }

        return unViewedMessages;
    } catch (error) {
        console.error('Error fetching chats:', error);
        throw error;
    }
}

async function getAllUnViewedTasksForUser(user_id) {
    try {
        return await Task.find({viewed: {$ne: user_id}});
    } catch (error) {
        console.error('Error fetching chats:', error);
        throw error;
    }
}

function getUser(id) {
    return ActiveUsers.users.find(user => user.id === id)
}



function arraysAreEqual(array1, array2) {
    if (array1.length !== array2.length) {
        return false;
    }
    const sortedArray1 = array1.slice().sort();
    const sortedArray2 = array2.slice().sort();
    return sortedArray1.every((value, index) => value === sortedArray2[index]);
}

