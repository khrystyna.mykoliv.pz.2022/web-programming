const lib = {}

// ================================ SHOW USER AND ROOM LISTS ================================
document.addEventListener("DOMContentLoaded", function() {
    socket.on('unReadMessages', ({ unViewedMessages }) => {
        let wrapNotifications = document.getElementById('notifications');
        let countNotifications = document.getElementById('count__notifications');

        if (unViewedMessages.length !== 0) {
            countNotifications.style.display = 'flex';
            countNotifications.textContent = unViewedMessages.length;
        }

        unViewedMessages.forEach(message => {
            const $message = document.createElement('div');
            $message.classList.add('notification');
            $message.innerHTML = `
                    <div class="notification__person">
                        <img src="${message.sender.avatar}" alt="notification-avatar">
                            <span class="notification__author">${message.sender.name}</span>
                    </div>
                    <div class="notification__message">
                        <p>${message.content}</p>
                    </div>
                `;

            wrapNotifications.appendChild($message);
        })

        if (unViewedMessages.length === 0) {
            wrapNotifications.innerHTML = `<h4>У вас немає повідомлень!</h4>`;
        }
    });

    socket.on('unViewedTasks', ({ unViewedTasks }) => {
        let wrapTasks = document.getElementById('tasks');
        let countTasks = document.getElementById('count__tasks');

        if (unViewedTasks.length !== 0) {
            countTasks.style.display = 'flex';
            countTasks.textContent = unViewedTasks.length;
        }

        unViewedTasks.forEach(task => {
            const $task = document.createElement('div');
            $task.classList.add('task');
            $task.innerHTML = `
                    <div class="task__person">
                        <span class="task__board">${task.board}</span>
                        <span class="task__author">${moment(task.date).format( 'MM.DD')}</span>
                    </div>
                    <div class="task__message">
                        <p>${task.name}</p>
                    </div>
                `;

            wrapTasks.appendChild($task);
        })

        console.log(unViewedTasks);

        if (unViewedTasks.length === 0) {
            wrapTasks.innerHTML = `<h4>Список завдань пустий!</h4>`;
        }
    })
});
