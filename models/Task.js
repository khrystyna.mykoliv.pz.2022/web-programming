import mongoose from 'mongoose';

const taskSchema = new mongoose.Schema({
    board: {
        type: String,
        enum: ['todo', 'in_process', 'done'],
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        required: true
    },
    viewed: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

// Создание модели на основе схемы
const Task = mongoose.model('Task', taskSchema);

export default Task;