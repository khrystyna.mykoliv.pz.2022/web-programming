import mongoose from 'mongoose';

const messageSchema = new mongoose.Schema({
    chat: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Chat'
    },
    sender: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    content: String,
    timestamp: {
        type: Date,
        default: Date.now
    },
    viewed: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }]
});

const Message = mongoose.model('Message', messageSchema);

export default Message;