let currentUser = {}

document.addEventListener("DOMContentLoaded", function() {
    socket.on('currentUser', ({ user }) => {
        currentUser.name = user.name;
        currentUser.avatar = user.avatar;
    })

    // ================================ VARIABLES ================================

    let rooms = document.getElementById('rooms');
    const emptyChat = document.getElementById('empty-room');
    const addGroupButton = document.getElementById('new-chat');

    function joinPrivateRoom(room) {
        socket.emit('EnterPrivateChat', { sender: currentUser.name, receiver: room.name.replace(currentUser.name, '').replace('private', '').split('_').join('') });
        socket.on('privateChatCreated', ({ sender, roomName }) => {
            if (sender === currentUser.name) {
                lib.chat(({
                    name: room.name,
                    backName: roomName,
                    type: 'private',
                    avatar: room.avatar,
                    socket: socket
                }));
            }
        })

        emptyChat.style.display = 'none';
    }

    function joinGroup(room, name) {
        console.log(room, name);
        socket.emit('EnterGroup', { name, room });
    }

    socket.on('GroupCreated', ({ name, room }) => {
        if (name === currentUser.name) {
            lib.chat(({
                name: room,
                type: 'group',
                backName: room,
                avatar: "../assets/images/delete-icon.svg",
                socket: socket
            }));

            emptyChat.style.display = 'none';
        }
    })

    addGroupButton.addEventListener('click', (e) => {
        lib.chat_form({
            title: 'New room',
            closable: true,
            width: window.innerWidth < 600 ? '90%' : '550px'
        }).then((data) => {
            socket.emit('createGroup', { name: currentUser.name, room: data });
        }).catch((err) => {
            console.log(err);
        });
    });

    socket.on('addGroupInList', ({ group, user }) => {
        addGroupRoom("../../assets/images/youravatar.svg", group, user.name);
    })

    // ================================ END ================================

    socket.on('roomList', ({ rooms }) => {
        showRooms(rooms)
    })

    function showRooms(dataRooms) {
        const privateRooms = document.querySelectorAll('.room.private');

        privateRooms.forEach(room => {
            room.remove();
        });
        console.log(dataRooms);

        if (!dataRooms.empty) {
            dataRooms.forEach((room) => {
                let roomNode = document.createElement('div');
                roomNode.classList.add('room');
                roomNode.classList.add('private');


                if (room.type === 'private') {
                    const friendName = room.name.replace(currentUser.name, '').replace('private', '').split('_').join('');
                    const friend = room.members.find(member => member.name === friendName);

                    if (!friend) {
                        return;
                    }
                    roomNode.innerHTML = `
                        <div class="room__avatar">
                            <img src="${friend.avatar}" alt="notification-avatar">
                        </div>
                        <h5 class="room__title">${friendName}</h5>
                    `
                    rooms.appendChild(roomNode);

                    roomNode.addEventListener('click', () => joinPrivateRoom(room));
                } else {
                    roomNode.innerHTML = `
                        <div class="room__avatar">
                            <img src="../../assets/images/youravatar.svg" alt="notification-avatar">
                        </div>
                        <h5 class="room__title">${room.name}</h5>
                    `
                    rooms.appendChild(roomNode);

                    roomNode.addEventListener('click', () => joinGroup(room.name, currentUser.name));
                }
            })
        }
    }

    function addGroupRoom(avatar, room, name) {
        if (document.getElementById(room)) {
            return
        }

        let roomNode = document.createElement('div');
        roomNode.classList.add('room');
        roomNode.id = room;

        roomNode.innerHTML = `
                    <div class="room__avatar">
                        <img src="${avatar}" alt="notification-avatar">
                    </div>
                    <h5 class="room__title">${room}</h5>
                `
        rooms.appendChild(roomNode);

        roomNode.addEventListener('click', () => joinGroup(room, name));
    }
});

