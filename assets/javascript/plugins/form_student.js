lib.student_form = function(options) {
    const form = `
        <form action="#" method="post" id="student_form">
            <div class="group__standard">
                <label for="group" class="standard__label">Group:</label>
                <div class="select-group">
                    <select id="group" name="group" class="standard__input">
                        <option value="PZ-25">PZ-25</option>
                        <option value="PZ-26">PZ-26</option>
                        <option value="PZ-27">PZ-27</option>
                    </select>
                </div>
            </div>
            <div class="group__standard">
                <label for="first_name" class="standard__label">First Name:</label>
                <input type="text" id="first_name" name="first_name" class="standard__input">
            </div>
            <div class="group__standard">
                <label for="last_name" class="standard__label">Last Name:</label>
                <input type="text" id="last_name" name="last_name" class="standard__input">
            </div>
            <div class="group__standard">
                <label for="gender" class="standard__label">Gender:</label>
                <div class="select-group">
                     <select id="gender" name="gender" class="standard__input">
                        <option value="M">Male</option>
                        <option value="F">Female</option>
                    </select>
                </div>
            </div>
            <div class="group__standard">
                <label for="birthday" class="standard__label">Birthday:</label>
                <input type="date" id="birthday" name="birthday" class="standard__input" value="2005-01-01">
            </div>
        </form>
    `

    return new Promise((resolve, reject) => {
        const modal = lib.modal({
            title: options.title,
            width: options.width,
            closable: options.closable,
            student: options.student || '',
            content: form,
            onClose() {
                modal.destroy();
            },
            footerButtons: [
                { text: options.button_action, type: 'success', handler() {
                        if ($("#student_form").valid()) {
                            let data = getDataStudent();

                            resolve(data);
                            modal.close();
                        }
                    }},
                { text: 'Cancel', type: 'danger', handler() {
                        modal.close();
                        reject();
                    }}
            ]
        });

        setTimeout(() => {modal.open()}, 100);

        $(document).ready(function() {
            if (options.student) {
                setStudentValues(options.student);
            }

            jQuery.validator.addMethod("youngerThan1985", function(value, element) {
                let inputDate = new Date(value);

                return inputDate.getFullYear() >= 1985;
            }, "The date must be no older than 1985");

            jQuery.validator.addMethod("olderThan2008", function(value, element) {
                let inputDate = new Date(value);

                return inputDate.getFullYear() <= 2008;
            }, "The date must be at least 2008");

            $("#student_form").validate({
                // rules: {
                //     group : {
                //         required: true
                //     },
                //     first_name : {
                //         required: true,
                //         minlength: 3
                //     },
                //     last_name : {
                //         required: true,
                //         minlength: 3
                //     },
                //     gender : {
                //         required : true
                //     },
                //     birthday : {
                //         required: true,
                //         date: true,
                //         youngerThan1985: true,
                //         olderThan2008: true
                //     }
                // },
                // messages : {
                //     first_name : {
                //         minlength: "Name should be at least 3 characters"
                //     },
                //     last_name : {
                //         minlength: "Surname should be at least 3 characters"
                //     }
                // }
            });
        });
    });
}

function getDataStudent() {
    const group = $('#group').val().trim();
    const first_name = $('#first_name').val().trim();
    const last_name = $('#last_name').val().trim();
    const gender = $('#gender').val().trim();
    const birthday = dayjs($('#birthday').val().trim());

    return {
        group: group,
        name: last_name + ' ' + first_name,
        gender: gender,
        birthday: birthday.format('DD.MM.YYYY')
    };
}

function setStudentValues(student) {
    $('#group').val(student.group);
    $('#first_name').val(student.name.split(" ")[1]);
    $('#last_name').val(student.name.split(" ")[0]);
    $('#gender').val(student.gender);
    $('#birthday').val(student.birthday.replace(/^(\d+)\.(\d+)\.(\d+)$/, "$3-$2-$1"));
}
