lib.chat_form = function(options) {
    const form = `
        <form action="" method="post" id="chat-room">
            <div class="group__standard">
                <label for="room-name" class="standard__label">Enter name of room:</label>
                <input type="text" id="room-name" name="room-name" class="standard__input">
            </div>
        </form>
    `;

    return new Promise((resolve, reject) => {
        const modal = lib.modal({
            title: 'New room',
            width: options.width,
            closable: options.closable,
            content: form,
            onClose() {
                modal.destroy();
            },
            footerButtons: [
                { text: 'Create', type: 'green', handler() {
                        const room = document.getElementById('room-name').value.trim();
                        resolve(room);
                        modal.close();
                    }},
                { text: 'Cancel', type: 'red', handler() {
                        modal.close();
                        reject();
                    }}
            ]
        });

        setTimeout(() => {modal.open()}, 300);
    });
}

lib.members_modal = function(options) {
    const userListContainer = document.createElement('div');
    const listUsers = document.createElement('div');
    listUsers.classList.add('available__users');
    listUsers.id = 'user-list';
    listUsers.innerHTML = '';
    userListContainer.appendChild(listUsers);

    options.availableUsers.forEach(user => {
        const userElement = document.createElement('div');
        userElement.classList.add('user');
        userElement.innerHTML = `
              <input type="checkbox" id="${user.name}" name="members" class="user__checkbox" value="${user.name}">
              <label for="${user.name}" class="user__label">
                    <img src="${user.avatar}" alt="${user.name}" class="user__img">
                    <h5 class="user__name">${user.name}</h5>
              </label>
        `;
        listUsers.appendChild(userElement);
    });

    if (userListContainer.innerHTML === '') {
        userListContainer.innerHTML = `<h4>there are no members available</h4>`;
    }

    return new Promise((resolve, reject) => {
        const modal = lib.modal({
            title: 'New members',
            width: options.width,
            closable: options.closable,
            content: userListContainer.innerHTML,
            onClose() {
                modal.destroy();
            },
            footerButtons: [
                { text: 'Add', type: 'green', handler() {
                        const members = getMembersFromForm();
                        resolve(members);
                        modal.close();
                    }},
                { text: 'Cancel', type: 'red', handler() {
                        modal.close();
                        reject();
                    }}
            ]
        });

        setTimeout(() => {modal.open()}, 300);
    });
}

function getMembersFromForm() {
    const membersInputs = document.getElementById('user-list').querySelectorAll('input[type="checkbox"]');
    const checkedMembers =  Array.from(membersInputs).filter(checkbox => checkbox.checked);
    let membersList = [];

    checkedMembers.forEach(member => {
        membersList.push(member.value);
    })

    return membersList;
}