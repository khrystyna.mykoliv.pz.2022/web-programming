function _createFlashMessage(options) {
    const flash_message = document.createElement('div');
    flash_message.classList.add('flash-message');
    flash_message.classList.add(options.type);
    flash_message.insertAdjacentHTML('afterbegin', `
      <p>${options.message}</p>
   `);
    document.body.insertBefore(flash_message, document.body.firstChild);

    return flash_message
}

lib.flash = function(options) {
    const $flash = _createFlashMessage(options);

    $flash.classList.add('open');
    const flash = new Promise(resolve => {
        setTimeout(() => {
            $flash.classList.add('hide');
            resolve();

        }, 1500);
    })

    flash.then(() => {
        $flash.classList.remove('open');
        setTimeout(() => {
            $flash.parentNode.removeChild($flash);
        }, 1500);
    })
}